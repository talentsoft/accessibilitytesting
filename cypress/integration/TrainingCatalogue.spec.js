import { TrainingCatalogRoutes } from "../support/constants";

  const terminalLog = (violations) => {
    cy.task(
      'log',
      `${violations.length} accessibility violation${
        violations.length === 1 ? '' : 's'
      } ${violations.length === 1 ? 'was' : 'were'} detected`
    )
    // pluck specific keys to keep the table readable
    const violationData = violations.map(
      ({ id, impact, description, nodes }) => ({
        id,
        impact,
        description,
        nodes: nodes.length
      })
    )
  
    cy.task('table', violationData)
  }

  before(() => {
    
Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false
})
    cy.visit("https://tests-boa-a11y-car.envs.test/");
    cy.login();
  })
  beforeEach(() => {
    Cypress.Cookies.preserveOnce('.TsAuth')
  })

  describe('Component accessibility test', () => {

    TrainingCatalogRoutes.forEach((route) => {
      const componentName = route.replace('.aspx', '');
      const testName = `${componentName} page has no detectable accessibility violations on load`;

      it(testName, () => {
        cy.visit(route)
        cy.wait(30000);
        cy.injectAxe();
        cy.checkA11y(null, null, terminalLog);
      })
    })

    it("Test More Details modal", ()=>{
      cy.visit("https://tests-boa-a11y-car.envs.test/MyTalentsoft#/Training/Catalogue/MyScope/Action/368")
      cy.wait(10000);
      cy.get('.secondary-btn').click();
      cy.wait(2000);
      cy.injectAxe();
      cy.checkA11y(null,null,terminalLog);
    })   
    
    it("Test Browse themes menu", ()=>{
      cy.visit("https://tests-boa-a11y-car.envs.test/MyTalentsoft#/Training/Catalogue/MyScope")
      cy.wait(10000);
      cy.get('.expandable-menu > .training-undefined-ts46 > .training-undefined-ts18').click();
      cy.wait(2000);
      cy.injectAxe();
      cy.checkA11y(null,null,terminalLog);
    }) 

    it("Test group requirments", ()=>{
      cy.visit("https://tests-boa-a11y-car.envs.test/MyTalentsoft#/Training/Catalogue/MyScope/Action/368")
      cy.wait(10000);
      cy.get('.training-undefined-ts6 > :nth-child(2) > .training-undefined-ts46 > .training-undefined-ts18').click();
      cy.wait(10000);
      cy.get('.group-btn > :nth-child(2)').click();
      cy.wait(2000);
      cy.injectAxe();
      cy.checkA11y(null,null,terminalLog);
    }) 

    it("Test request page", ()=>{
      cy.visit("https://tests-boa-a11y-car.envs.test/MyTalentsoft#/Training/Catalogue/MyScope/Action/368")
      cy.wait(10000);
      cy.get('.training-undefined-ts6 > :nth-child(2) > .training-undefined-ts46 > .training-undefined-ts18').click();
      cy.wait(2000);
      cy.injectAxe();
      cy.checkA11y(null,null,terminalLog);
    }) 

    it("Test registration page", ()=>{
      cy.visit("https://tests-boa-a11y-car.envs.test/MyTalentsoft#/Training/Catalogue/MyScope/Action/368")
      cy.wait(10000);
      cy.get(':nth-child(1) > .training-undefined-ts46 > .training-undefined-ts18').click();
      cy.wait(2000);
      cy.injectAxe();
      cy.checkA11y(null,null,terminalLog);
    }) 
  })